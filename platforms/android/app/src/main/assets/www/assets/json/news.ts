export const newsSalud= [
    {
        id:0,
        img: 'https://www.eltiempo.com/files/article_main/uploads/2020/12/02/5fc79488a484f.jpeg',
        title:'Vacuna de Pfizer es eficaz y segura: ‘New England Journal of Medicine’',
        subTitle:'SALUD DICIEMBRE 10 DE 2020',
        bodyNew: ' La revista científica New England Journal of Medicine (NEJM), una de las más prestigiosas del mundo, publicó este jueves los resultados completos de los ensayos de fase III del proyecto de vacuna BNT162b2 contra el covid-19 de Pfizer y BioNTech, en los que se confirma que este fármaco tiene una eficacia del 95 por ciento y ofrece condiciones de seguridad para aplicarlo masivamente.' +
        '(Le recomendamos: 10 cosas que se deben saber sobre la vacuna de Pfizer contra covid-19) '+
        'La publicación se refiere en un editorial a un "triunfo", en momentos en que un comité de expertos convocados por la Agencia estadounidense de Medicamentos (FDA) comenzó a examinar en público este mismo jueves los datos de la vacuna, con el objetivo de recomendar o no su autorización de lanzamiento al mercado.'
    },
    {
        id:1,
        img: 'https://s.france24.com/media/display/3859d94c-229d-11eb-a822-005056bff430/w:900/p:16x9/2020-11-09T114528Z_401439984_RC2NZJ9UB98H_RTRMADP_3_HEALTH-CORONAVIRUS-VACCINES-PFIZER.webp',
        title:'FDA autoriza vacuna de Pfizer contra covid-19 en Estados Unidos',
        subTitle:'12 de diciembre 2020 , 12:00 a. m.',
        bodyNew: 'La Administración de Medicamentos y Alimentos de Estados Unidos (FDA) autorizó la vacuna contra el coronavirus de Pfizer para uso de emergencia, lo que allanó el camino para que millones de estadounidenses altamente vulnerables la contraigan en unos días, señaló New York Times.'+

        +'FDA ya había dicho trabajaba "rápidamente" para autorizar el uso de emergencia de la vacuna contra el covid-19 de Pfizer/BioNTech, después de que un comité asesor analizó en profundidad los resultados de la investigación presentados por las empresas y les dio el visto bueno.'
    },
    {
        id:2,
        img: 'https://s.france24.com/media/display/3859d94c-229d-11eb-a822-005056bff430/w:900/p:16x9/2020-11-09T114528Z_401439984_RC2NZJ9UB98H_RTRMADP_3_HEALTH-CORONAVIRUS-VACCINES-PFIZER.webp',
        title:'FDA autoriza vacuna de Pfizer contra covid-19 en Estados Unidos',
        subTitle:'12 de diciembre 2020 , 12:00 a. m.',
        bodyNew: 'La Administración de Medicamentos y Alimentos de Estados Unidos (FDA) autorizó la vacuna contra el coronavirus de Pfizer para uso de emergencia, lo que allanó el camino para que millones de estadounidenses altamente vulnerables la contraigan en unos días, señaló New York Times.'+

        +'FDA ya había dicho trabajaba "rápidamente" para autorizar el uso de emergencia de la vacuna contra el covid-19 de Pfizer/BioNTech, después de que un comité asesor analizó en profundidad los resultados de la investigación presentados por las empresas y les dio el visto bueno.'
    }
];


export const newsDeportes= [
    {
        id:0,
        img: 'https://www.eltiempo.com/files/main_home_image/uploads/2020/01/12/5e1b6b5c03caa.jpeg',
        title:'EN VIVO: siga el minuto a minuto del Real Madrid vs. Atlético',
        subTitle:'DEPORTES DICIEMBRE 10 DE 2020',
        bodyNew: 'El Atlético de Madrid encara un derbi liguero de la mayor dimensión posible gracias a su crecimiento futbolístico este curso, con la posibilidad de cambiar su papel de aspirante a favorito si vence a un Real Madrid renacido cuando sintió de cerca el abismo, obligado a puntuar si no quiere seguir los pasos del Barcelona y sentir insalvable la distancia que adquiera el líder. '
    }
];