(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab3-tab3-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-title>\n      Calculadora\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n  \n  <ion-grid>\n    <ion-row>\n      <ion-col class=\"display\">\n        {{display}}\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col class=\"btn-numbers\" (click)=\"fnNumbers(7)\">\n       7\n      </ion-col>\n      <ion-col class=\"btn-numbers\" (click)=\"fnNumbers(8)\">\n        8\n      </ion-col>\n      <ion-col class=\"btn-numbers\" (click)=\"fnNumbers(9)\">\n       9\n      </ion-col>\n      <ion-col class=\"btn-oper\" (click)=\"operation('/')\">\n       /\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row>\n      <ion-col class=\"btn-numbers\" (click)=\"fnNumbers(4)\">\n       4\n      </ion-col>\n      <ion-col class=\"btn-numbers\" (click)=\"fnNumbers(5)\">\n        5\n      </ion-col>\n      <ion-col class=\"btn-numbers\" (click)=\"fnNumbers(6)\">\n       6\n      </ion-col>\n      <ion-col class=\"btn-oper\" (click)=\"operation('x')\">\n       x\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row>\n      <ion-col class=\"btn-numbers\" (click)=\"fnNumbers(1)\">\n       1\n      </ion-col>\n      <ion-col class=\"btn-numbers\" (click)=\"fnNumbers(2)\">\n        2\n      </ion-col>\n      <ion-col class=\"btn-numbers\" (click)=\"fnNumbers(3)\">\n       3\n      </ion-col>\n      <ion-col class=\"btn-oper\" (click)=\"operation('-')\">\n       -\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row>\n      <ion-col  class=\"btn-numbers\" (click)=\"fnNumbers(0)\">\n       0\n      </ion-col>\n      <ion-col class=\"btn-oper\" (click)=\"clear()\">\n        C\n      </ion-col>\n      <ion-col class=\"btn-oper\" (click)=\"equal()\">\n        =\n      </ion-col>\n      <ion-col class=\"btn-oper\" (click)=\"operation('+')\">\n       +\n      </ion-col>\n    </ion-row>\n\n\n  </ion-grid>\n\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/tab3/tab3-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/tab3/tab3-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab3PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageRoutingModule", function() { return Tab3PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab3.page */ "./src/app/tab3/tab3.page.ts");




const routes = [
    {
        path: '',
        component: _tab3_page__WEBPACK_IMPORTED_MODULE_3__["Tab3Page"],
    }
];
let Tab3PageRoutingModule = class Tab3PageRoutingModule {
};
Tab3PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Tab3PageRoutingModule);



/***/ }),

/***/ "./src/app/tab3/tab3.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.module.ts ***!
  \*************************************/
/*! exports provided: Tab3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageModule", function() { return Tab3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab3.page */ "./src/app/tab3/tab3.page.ts");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../explore-container/explore-container.module */ "./src/app/explore-container/explore-container.module.ts");
/* harmony import */ var _tab3_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./tab3-routing.module */ "./src/app/tab3/tab3-routing.module.ts");









let Tab3PageModule = class Tab3PageModule {
};
Tab3PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_7__["ExploreContainerComponentModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"] }]),
            _tab3_routing_module__WEBPACK_IMPORTED_MODULE_8__["Tab3PageRoutingModule"],
        ],
        declarations: [_tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"]]
    })
], Tab3PageModule);



/***/ }),

/***/ "./src/app/tab3/tab3.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".display {\n  font-family: \"Roboto Condensed\", sans-serif;\n  color: rgba(255, 255, 255, 0.8);\n  background: rgba(0, 0, 0, 0.6);\n  font-size: 35px;\n  padding: 10px;\n  text-align: right;\n  border-radius: 2px;\n  box-shadow: inset 0 5px 8px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3);\n  margin-bottom: 5px;\n}\n\n.btn-numbers {\n  width: 30px;\n  height: 55px;\n  margin: 2px;\n  background-color: #c3c3c3;\n  border-radius: 12px;\n  text-align: center;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  font-size: 24px;\n}\n\n.btn-oper {\n  width: 30px;\n  height: 55px;\n  margin: 2px;\n  background-color: #cf8414;\n  border-radius: 12px;\n  text-align: center;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  font-size: 24px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFiMy90YWIzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLDJDQUFBO0VBQ0EsK0JBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdGQUFBO0VBQ0Esa0JBQUE7QUFESjs7QUFLQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtBQUZKOztBQU1BO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0FBSEoiLCJmaWxlIjoic3JjL2FwcC90YWIzL3RhYjMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG5cbi5kaXNwbGF5e1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvIENvbmRlbnNlZCcsIHNhbnMtc2VyaWY7XG4gICAgY29sb3I6IHJnYmEoMjU1LDI1NSwyNTUsMC44KTtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLC42KTtcbiAgICBmb250LXNpemU6IDM1cHg7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgYm94LXNoYWRvdzogaW5zZXQgMCA1cHggOHB4IHJnYmEoMCwwLDAsMC4zKSwgMCAxcHggMCByZ2JhKDI1NSwyNTUsMjU1LDAuMyk7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4OyBcbn1cblxuXG4uYnRuLW51bWJlcnN7XG4gICAgd2lkdGg6IDMwcHg7XG4gICAgaGVpZ2h0OiA1NXB4O1xuICAgIG1hcmdpbjogMnB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNjM2MzYzM7XG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZvbnQtc2l6ZTogMjRweDtcbn1cblxuXG4uYnRuLW9wZXJ7XG4gICAgd2lkdGg6IDMwcHg7XG4gICAgaGVpZ2h0OiA1NXB4O1xuICAgIG1hcmdpbjogMnB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNjZjg0MTQ7XG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZvbnQtc2l6ZTogMjRweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/tab3/tab3.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab3/tab3.page.ts ***!
  \***********************************/
/*! exports provided: Tab3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3Page", function() { return Tab3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let Tab3Page = class Tab3Page {
    constructor() {
        this.memory = [];
        this.display = 0;
    }
    fnNumbers(n) {
        this.memory.push(n);
        this.display = Number(this.memory.join(''));
    }
    clear() {
        this.display = 0;
        this.memory = [];
    }
    operation(s) {
        this.operand = this.display;
        this.simbolo = s;
        this.memory = [];
    }
    equal() {
        switch (this.simbolo) {
            case '+':
                this.display = this.operand + this.display;
                break;
            case '-':
                this.display = this.operand - this.display;
                break;
            case 'x':
                this.display = this.operand * this.display;
                break;
            case '/':
                this.display = this.operand / this.display;
                break;
            default:
                break;
        }
    }
};
Tab3Page.ctorParameters = () => [];
Tab3Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab3',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./tab3.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./tab3.page.scss */ "./src/app/tab3/tab3.page.scss")).default]
    })
], Tab3Page);



/***/ })

}]);
//# sourceMappingURL=tab3-tab3-module-es2015.js.map