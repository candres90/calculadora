import { Component } from '@angular/core';
import { newsDeportes } from '../../assets/json/news';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  arrayNews = newsDeportes;

  constructor() {}

}
