import { Component } from '@angular/core';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  memory = [];
  display = 0;
  operand;
  simbolo;


  constructor() {}


  fnNumbers(n){
    this.memory.push(n);
    this.display = Number(this.memory.join(''));
  }

  clear(){
    this.display = 0;
    this.memory = [];
  }

  operation(s){
    this.operand = this.display;
    this.simbolo = s;
    this.memory = [];
  }

  equal(){
   switch (this.simbolo) {
     case '+':
       this.display = this.operand + this.display;
       break;
    case '-':
      this.display = this.operand - this.display;
        break;
    case 'x':
      this.display = this.operand * this.display;
        break;
    case '/':
      this.display = this.operand / this.display;
        break;
   
     default:
       break;
   }
  }

}
